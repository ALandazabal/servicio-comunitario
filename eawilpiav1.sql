-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-06-2018 a las 20:18:47
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `eawilpia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adolescente`
--

CREATE TABLE `adolescente` (
  `idAdolescente` int(11) NOT NULL,
  `nombreA` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `apellidoA` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `fkNac` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `adolescente`
--

INSERT INTO `adolescente` (`idAdolescente`, `nombreA`, `apellidoA`, `fkNac`) VALUES
(111, 'Camila', 'Cabello', 1),
(123, 'hola', 'hoal', 1),
(222, 'Helen', 'Havana', 1),
(678, 'Morita2', 'Mora', 1),
(2345, 'martha', 'mora', 1),
(13456, 'carla', 'jimenes', 1),
(34563, 'rosa', 'mora', 2),
(123456, 'ana', 'salas', 1),
(212121, 'martha', 'mora', 2),
(20202020, 'Maria', 'DB', 1),
(1234567890, 'cordoba', 'caceres', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `idEstado` int(11) NOT NULL,
  `descripcionE` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `Pais_idPais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`idEstado`, `descripcionE`, `Pais_idPais`) VALUES
(1, 'Amazonas', 2),
(2, 'Anzoátegui', 2),
(3, 'Apure', 2),
(4, 'Aragua', 2),
(5, 'Barinas', 2),
(6, 'Bolívar', 2),
(7, 'Carabobo', 2),
(8, 'Cojedes', 2),
(9, 'Delta Amacuro', 2),
(10, 'Falcón', 2),
(11, 'Guárico', 2),
(12, 'Lara', 2),
(13, 'Mérida', 2),
(14, 'Miranda', 2),
(15, 'Monagas', 2),
(16, 'Nueva Esparta', 2),
(17, 'Portuguesa', 2),
(18, 'Sucre', 2),
(19, 'Táchira', 2),
(20, 'Trujillo', 2),
(21, 'Vargas', 2),
(22, 'Yaracuy', 2),
(23, 'Zulia', 2),
(24, 'Distrito Capital', 2),
(25, 'Dependencias \r\n\r\nFederales', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `idMunicipio` int(11) NOT NULL,
  `descripcionM` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `fkEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`idMunicipio`, `descripcionM`, `fkEstado`) VALUES
(1, 'Alto \r\n\r\nOrinoco', 1),
(2, 'Atabapo', 1),
(3, 'Atures', 1),
(4, 'Autana', 1),
(5, 'Manapiare', 1),
(6, 'Maroa', 1),
(7, 'Río Negro', 1),
(8, 'Anaco', 2),
(9, 'Aragua', 2),
(10, 'Manuel Ezequiel Bruzual', 2),
(11, 'Diego Bautista \r\n\r\nUrbaneja', 2),
(12, 'Fernando Peñalver', 2),
(13, 'Francisco \r\n\r\nDel Carmen Carvajal', 2),
(14, 'General Sir Arthur \r\n\r\nMcGregor', 2),
(15, 'Guanta', 2),
(16, 'Independencia', 2),
(17, 'José Gregorio Monagas', 2),
(18, 'Juan Antonio \r\n\r\nSotillo', 2),
(19, 'Juan Manuel Cajigal', 2),
(20, 'Libertad', 2),
(21, 'Francisco de Miranda', 2),
(22, 'Pedro María Freites', 2),
(23, 'Píritu', 2),
(24, 'San José de Guanipa', 2),
(25, 'San Juan de \r\n\r\nCapistrano', 2),
(26, 'Santa Ana', 2),
(27, 'Simón \r\n\r\nBolívar', 2),
(28, 'Simón Rodríguez', 2),
(29, 'Achaguas', 3),
(30, 'Biruaca', 3),
(31, 'Muñóz', 3),
(32, 'Páez', 3),
(33, 'Pedro Camejo', 3),
(34, 'Rómulo Gallegos', 3),
(35, 'San Fernando', 3),
(36, 'Atanasio Girardot', 4),
(37, 'Bolívar', 4),
(38, 'Camatagua', 4),
(39, 'Francisco \r\n\r\nLinares Alcántara', 4),
(40, 'José Ángel Lamas', 4),
(41, 'José Félix Ribas', 4),
(42, 'José Rafael \r\n\r\nRevenga', 4),
(43, 'Libertador', 4),
(44, 'Mario Briceño \r\n\r\nIragorry', 4),
(45, 'Ocumare de la Costa de Oro', 4),
(46, 'San Casimiro', 4),
(47, 'San Sebastián', 4),
(48, 'Santiago Mariño', 4),
(49, 'Santos Michelena', 4),
(50, 'Sucre', 4),
(51, 'Tovar', 4),
(52, 'Urdaneta', 4),
(53, 'Zamora', 4),
(54, 'Alberto Arvelo Torrealba', 5),
(55, 'Andrés Eloy Blanco', 5),
(56, 'Antonio José de \r\n\r\nSucre', 5),
(57, 'Arismendi', 5),
(58, 'Barinas', 5),
(59, 'Bolívar', 5),
(60, 'Cruz Paredes', 5),
(61, 'Ezequiel \r\n\r\nZamora', 5),
(62, 'Obispos', 5),
(63, 'Pedraza', 5),
(64, 'Rojas', 5),
(65, 'Sosa', 5),
(66, 'Caroní', 6),
(67, 'Cedeño', 6),
(68, 'El Callao', 6),
(69, 'Gran \r\n\r\nSabana', 6),
(70, 'Heres', 6),
(71, 'Piar', 6),
(72, 'Angostura \r\n\r\n(Raúl Leoni)', 6),
(73, 'Roscio', 6),
(74, 'Sifontes', 6),
(75, 'Sucre', 6),
(76, 'Padre Pedro Chien', 6),
(77, 'Bejuma', 7),
(78, 'Carlos Arvelo', 7),
(79, 'Diego \r\n\r\nIbarra', 7),
(80, 'Guacara', 7),
(81, 'Juan José Mora', 7),
(82, 'Libertador', 7),
(83, 'Los Guayos', 7),
(84, 'Miranda', 7),
(85, 'Montalbán', 7),
(86, 'Naguanagua', 7),
(87, 'Puerto Cabello', 7),
(88, 'San \r\n\r\nDiego', 7),
(89, 'San Joaquín', 7),
(90, 'Valencia', 7),
(91, 'Anzoátegui', 8),
(92, 'Tinaquillo', 8),
(93, 'Girardot', 8),
(94, 'Lima Blanco', 8),
(95, 'Pao de \r\n\r\nSan Juan Bautista', 8),
(96, 'Ricaurte', 8),
(97, 'Rómulo \r\n\r\nGallegos', 8),
(98, 'San Carlos', 8),
(99, 'Tinaco', 8),
(100, 'Antonio Díaz', 9),
(101, 'Casacoima', 9),
(102, 'Pedernales', 9),
(103, 'Tucupita', 9),
(104, 'Acosta', 10),
(105, 'Bolívar', 10),
(106, 'Buchivacoa', 10),
(107, 'Cacique Manaure', 10),
(108, 'Carirubana', 10),
(109, 'Colina', 10),
(110, 'Dabajuro', 10),
(111, 'Democracia', 10),
(112, 'Falcón', 10),
(113, 'Federación', 10),
(114, 'Jacura', 10),
(115, 'José Laurencio Silva', 10),
(116, 'Los Taques', 10),
(117, 'Mauroa', 10),
(118, 'Miranda', 10),
(119, 'Monseñor Iturriza', 10),
(120, 'Palmasola', 10),
(121, 'Petit', 10),
(122, 'Píritu', 10),
(123, 'San Francisco', 10),
(124, 'Sucre', 10),
(125, 'Tocópero', 10),
(126, 'Unión', 10),
(127, 'Urumaco', 10),
(128, 'Zamora', 10),
(129, 'Camaguán', 11),
(130, 'Chaguaramas', 11),
(131, 'El Socorro', 11),
(132, 'José Félix Ribas', 11),
(133, 'José Tadeo \r\n\r\nMonagas', 11),
(134, 'Juan Germán Roscio', 11),
(135, 'Julián Mellado', 11),
(136, 'Las Mercedes', 11),
(137, 'Leonardo Infante', 11),
(138, 'Pedro Zaraza', 11),
(139, 'Ortíz', 11),
(140, 'San Gerónimo de Guayabal', 11),
(141, 'San José de Guaribe', 11),
(142, 'Santa María de \r\n\r\nIpire', 11),
(143, 'Sebastián Francisco de Miranda', 11),
(144, 'Andrés Eloy Blanco', 12),
(145, 'Crespo', 12),
(146, 'Iribarren', 12),
(147, 'Jiménez', 12),
(148, 'Morán', 12),
(149, 'Palavecino', 12),
(150, 'Simón \r\n\r\nPlanas', 12),
(151, 'Torres', 12),
(152, 'Urdaneta', 12),
(179, 'Alberto Adriani', 13),
(180, 'Andrés Bello', 13),
(181, 'Antonio Pinto Salinas', 13),
(182, 'Aricagua', 13),
(183, 'Arzobispo Chacón', 13),
(184, 'Campo Elías', 13),
(185, 'Caracciolo Parra Olmedo', 13),
(186, 'Cardenal \r\n\r\nQuintero', 13),
(187, 'Guaraque', 13),
(188, 'Julio César \r\n\r\nSalas', 13),
(189, 'Justo Briceño', 13),
(190, 'Libertador', 13),
(191, 'Miranda', 13),
(192, 'Obispo Ramos de Lora', 13),
(193, 'Padre \r\n\r\nNoguera', 13),
(194, 'Pueblo Llano', 13),
(195, 'Rangel', 13),
(196, 'Rivas Dávila', 13),
(197, 'Santos Marquina', 13),
(198, 'Sucre', 13),
(199, 'Tovar', 13),
(200, 'Tulio \r\n\r\nFebres Cordero', 13),
(201, 'Zea', 13),
(223, 'Acevedo', 14),
(224, 'Andrés Bello', 14),
(225, 'Baruta', 14),
(226, 'Brión', 14),
(227, 'Buroz', 14),
(228, 'Carrizal', 14),
(229, 'Chacao', 14),
(230, 'Cristóbal Rojas', 14),
(231, 'El Hatillo', 14),
(232, 'Guaicaipuro', 14),
(233, 'Independencia', 14),
(234, 'Lander', 14),
(235, 'Los Salias', 14),
(236, 'Páez', 14),
(237, 'Paz Castillo', 14),
(238, 'Pedro \r\n\r\nGual', 14),
(239, 'Plaza', 14),
(240, 'Simón Bolívar', 14),
(241, 'Sucre', 14),
(242, 'Urdaneta', 14),
(243, 'Zamora', 14),
(258, 'Acosta', 15),
(259, 'Aguasay', 15),
(260, 'Bolívar', 15),
(261, 'Caripe', 15),
(262, 'Cedeño', 15),
(263, 'Ezequiel \r\n\r\nZamora', 15),
(264, 'Libertador', 15),
(265, 'Maturín', 15),
(266, 'Piar', 15),
(267, 'Punceres', 15),
(268, 'Santa \r\n\r\nBárbara', 15),
(269, 'Sotillo', 15),
(270, 'Uracoa', 15),
(271, 'Antolín del Campo', 16),
(272, 'Arismendi', 16),
(273, 'García', 16),
(274, 'Gómez', 16),
(275, 'Maneiro', 16),
(276, 'Marcano', 16),
(277, 'Mariño', 16),
(278, 'Península de Macanao', 16),
(279, 'Tubores', 16),
(280, 'Villalba', 16),
(281, 'Díaz', 16),
(282, 'Agua Blanca', 17),
(283, 'Araure', 17),
(284, 'Esteller', 17),
(285, 'Guanare', 17),
(286, 'Guanarito', 17),
(287, 'Monseñor José Vicente de Unda', 17),
(288, 'Ospino', 17),
(289, 'Páez', 17),
(290, 'Papelón', 17),
(291, 'San Genaro de Boconoíto', 17),
(292, 'San Rafael \r\n\r\nde Onoto', 17),
(293, 'Santa Rosalía', 17),
(294, 'Sucre', 17),
(295, 'Turén', 17),
(296, 'Andrés Eloy Blanco', 18),
(297, 'Andrés Mata', 18),
(298, 'Arismendi', 18),
(299, 'Benítez', 18),
(300, 'Bermúdez', 18),
(301, 'Bolívar', 18),
(302, 'Cajigal', 18),
(303, 'Cruz \r\n\r\nSalmerón Acosta', 18),
(304, 'Libertador', 18),
(305, 'Mariño', 18),
(306, 'Mejía', 18),
(307, 'Montes', 18),
(308, 'Ribero', 18),
(309, 'Sucre', 18),
(310, 'Valdéz', 18),
(341, 'Andrés Bello', 19),
(342, 'Antonio Rómulo \r\n\r\nCosta', 19),
(343, 'Ayacucho', 19),
(344, 'Bolívar', 19),
(345, 'Cárdenas', 19),
(346, 'Córdoba', 19),
(347, 'Fernández Feo', 19),
(348, 'Francisco de \r\n\r\nMiranda', 19),
(349, 'García de Hevia', 19),
(350, 'Guásimos', 19),
(351, 'Independencia', 19),
(352, 'Jáuregui', 19),
(353, 'José María Vargas', 19),
(354, 'Junín', 19),
(355, 'Libertad', 19),
(356, 'Libertador', 19),
(357, 'Lobatera', 19),
(358, 'Michelena', 19),
(359, 'Panamericano', 19),
(360, 'Pedro María Ureña', 19),
(361, 'Rafael \r\n\r\nUrdaneta', 19),
(362, 'Samuel Darío Maldonado', 19),
(363, 'San Cristóbal', 19),
(364, 'Seboruco', 19),
(365, 'Simón Rodríguez', 19),
(366, 'Sucre', 19),
(367, 'Torbes', 19),
(368, 'Uribante', 19),
(369, 'San \r\n\r\nJudas Tadeo', 19),
(370, 'Andrés Bello', 20),
(371, 'Boconó', 20),
(372, 'Bolívar', 20),
(373, 'Candelaria', 20),
(374, 'Carache', 20),
(375, 'Escuque', 20),
(376, 'José Felipe Márquez \r\n\r\nCañizalez', 20),
(377, 'Juan Vicente Campos Elías', 20),
(378, 'La Ceiba', 20),
(379, 'Miranda', 20),
(380, 'Monte \r\n\r\nCarmelo', 20),
(381, 'Motatán', 20),
(382, 'Pampán', 20),
(383, 'Pampanito', 20),
(384, 'Rafael Rangel', 20),
(385, 'San Rafael de Carvajal', 20),
(386, 'Sucre', 20),
(387, 'Trujillo', 20),
(388, 'Urdaneta', 20),
(389, 'Valera', 20),
(390, 'Vargas', 21),
(391, 'Arístides \r\n\r\nBastidas', 22),
(392, 'Bolívar', 22),
(407, 'Bruzual', 22),
(408, 'Cocorote', 22),
(409, 'Independencia', 22),
(410, 'José Antonio Páez', 22),
(411, 'La Trinidad', 22),
(412, 'Manuel Monge', 22),
(413, 'Nirgua', 22),
(414, 'Peña', 22),
(415, 'San Felipe', 22),
(416, 'Sucre', 22),
(417, 'Urachiche', 22),
(418, 'José \r\n\r\nJoaquín Veroes', 22),
(441, 'Almirante Padilla', 23),
(442, 'Baralt', 23),
(443, 'Cabimas', 23),
(444, 'Catatumbo', 23),
(445, 'Colón', 23),
(446, 'Francisco Javier Pulgar', 23),
(447, 'Páez', 23),
(448, 'Jesús Enrique Losada', 23),
(449, 'Jesús María \r\n\r\nSemprún', 23),
(450, 'La Cañada de Urdaneta', 23),
(451, 'Lagunillas', 23),
(452, 'Machiques de Perijá', 23),
(453, 'Mara', 23),
(454, 'Maracaibo', 23),
(455, 'Miranda', 23),
(456, 'Rosario de Perijá', 23),
(457, 'San Francisco', 23),
(458, 'Santa Rita', 23),
(459, 'Simón Bolívar', 23),
(460, 'Sucre', 23),
(461, 'Valmore Rodríguez', 23),
(462, 'Libertador', 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nacionalidad`
--

CREATE TABLE `nacionalidad` (
  `idNacionalidad` int(11)  unsigned default not null auto_increment,
  `descripcionN` varchar(45) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `nacionalidad`
--

INSERT INTO `nacionalidad` (`idNacionalidad`, `descripcionN`) VALUES
(1, 'Venezolana'),
(2, 'Colombiana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `idPais` int(11) NOT NULL,
  `descripcionP` varchar(45) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`idPais`, `descripcionP`) VALUES
(3, 'Colombia'),
(1, 'otro'),
(2, 'Venezuela');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `relacion`
--

CREATE TABLE `relacion` (
  `fkParentesco` int(11) NOT NULL,
  `fkVisitante` int(11) NOT NULL,
  `fkAdolescente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `relacion`
--

INSERT INTO `relacion` (`fkParentesco`, `fkVisitante`, `fkAdolescente`) VALUES
(1, 123, 123),
(1, 456, 1234567890),
(1, 212121, 20202020),
(2, 456, 222),
(2, 10111222, 2345),
(3, 456, 111),
(3, 456, 678),
(3, 212121, 2345),
(3, 10111222, 34563),
(3, 10111222, 20202020),
(3, 12345656, 2345),
(4, 212121, 212121),
(4, 10111222, 123456),
(4, 12345656, 20202020);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parentesco`
--

CREATE TABLE `parentesco` (
  `idParentesco` int(11) unsigned default not null auto_increment,
  `descripcionP` varchar(45) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `parentesco`
--

INSERT INTO `parentesco` (`idParentesco`, `descripcionP`) VALUES
(1, 'Padre'),
(2, 'Madre'),
(3, 'Abuela/o'),
(4, 'Tía/o'),
(5, 'Hija/o'),
(6, 'Representante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `clave` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `clave`) VALUES
(11, '33'),
(10177862, '123'),
(20626929, '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visita`
--

CREATE TABLE `visita` (
  `fecha` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `h_entrada` time NOT NULL,
  `h_salida` time DEFAULT NULL,
  `fkUsuario` int(11) NOT NULL,
  `fkRelVte` int(11) NOT NULL,
  `fkRelAdol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `visita`
--

INSERT INTO `visita` (`fecha`, `h_entrada`, `h_salida`, `fkUsuario`, `fkRelVte`, `fkRelAdol`) VALUES
('0000-00-00', '05:13:29', '05:50:02', 20626929, 456, 111),
('20-06-2018', '08:55:56', '08:58:40', 20626929, 456, 111),
('20-06-2018', '05:50:32', '08:20:35', 20626929, 456, 222),
('20-06-2018', '09:30:09', NULL, 20626929, 456, 678),
('20-06-2018', '05:49:09', '05:50:10', 20626929, 456, 1234567890),
('2018-06-07', '10:37:39', '10:38:46', 10177862, 456, 111),
('2018-06-07', '10:38:11', '05:49:08', 10177862, 456, 678),
('21-06-2018', '01:43:49', NULL, 10177862, 123, 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitante`
--

CREATE TABLE `visitante` (
  `idVisitante` int(11) NOT NULL,
  `nombreV` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `apellidoV` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `direccion` varchar(45) COLLATE latin1_spanish_ci NOT NULL,
  `fkMunicipio` int(11) NOT NULL,
  `fkNac` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `visitante`
--

INSERT INTO `visitante` (`idVisitante`, `nombreV`, `apellidoV`, `direccion`, `fkMunicipio`, `fkNac`) VALUES
(123, 'asd', 'asd', 'sd', 151, 1),
(456, 'Morita', 'Mora', 'Pueblo Nuevo', 379, 1),
(2345, 'Joser', 'velez', 'barrio obrero', 308, 1),
(56756, 'pedro', 'lopez', 'barrio obrero', 264, 1),
(212121, 'ana', 'velez', 'centro', 15, 1),
(578896, 'carla', 'garcia', 'Edif. Coro', 110, 1),
(2456467, 'carla', 'Piccasso', 'Edif. Coro', 350, 2),
(10111222, 'Pedro', 'Perez', 'Pueblo Nuevo', 457, 1),
(10150335, 'Pablo', 'Piccasso', 'Centro', 58, 1),
(12345656, 'ana', 'velez', 'centro', 27, 1),
(123456789, 'Joser', 'Perez', 'Caracas', 462, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adolescente`
--
ALTER TABLE `adolescente`
  ADD PRIMARY KEY (`idAdolescente`),
  ADD KEY `fk_Adolescente_Nacionalidad1_idx` (`fkNac`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idEstado`),
  ADD KEY `fk_Estado_Pais1_idx` (`Pais_idPais`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`idMunicipio`),
  ADD KEY `fk_Municipio_Estado_idx` (`fkEstado`);

--
-- Indices de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  ADD PRIMARY KEY (`idNacionalidad`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`idPais`),
  ADD UNIQUE KEY `descripcionP_UNIQUE` (`descripcionP`);

--
-- Indices de la tabla `relacion`
--
ALTER TABLE `relacion`
  ADD PRIMARY KEY (`fkVisitante`,`fkAdolescente`),
  ADD KEY `fk_Relacion_Parentesco1_idx` (`fkParentesco`),
  ADD KEY `fk_Relacion_Visitante1_idx` (`fkVisitante`),
  ADD KEY `fk_Relacion_Adolescente1_idx` (`fkAdolescente`);

--
-- Indices de la tabla `parentesco`
--
ALTER TABLE `parentesco`
  ADD PRIMARY KEY (`idParentesco`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indices de la tabla `visita`
--
ALTER TABLE `visita`
  ADD PRIMARY KEY (`fecha`,`fkRelVte`,`fkRelAdol`),
  ADD KEY `fk_Visita_Usuario1_idx` (`fkUsuario`),
  ADD KEY `fk_Visita_Relacion1_idx` (`fkRelVte`,`fkRelAdol`);

--
-- Indices de la tabla `visitante`
--
ALTER TABLE `visitante`
  ADD PRIMARY KEY (`idVisitante`),
  ADD KEY `fk_Visitante_Municipio1_idx` (`fkMunicipio`),
  ADD KEY `fk_Visitante_Nacionalidad1_idx` (`fkNac`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
  MODIFY `idMunicipio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=463;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `idPais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `adolescente`
--
ALTER TABLE `adolescente`
  ADD CONSTRAINT `fk_Adolescente_Nacionalidad` FOREIGN KEY (`fkNac`) REFERENCES `nacionalidad` (`idNacionalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `estado`
--
ALTER TABLE `estado`
  ADD CONSTRAINT `fk_Estado_Pais1` FOREIGN KEY (`Pais_idPais`) REFERENCES `pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD CONSTRAINT `fk_Municipio_Estado` FOREIGN KEY (`fkEstado`) REFERENCES `estado` (`idEstado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `relacion`
--
ALTER TABLE `relacion`
  ADD CONSTRAINT `fk_Relacion_Adolescente` FOREIGN KEY (`fkAdolescente`) REFERENCES `adolescente` (`idAdolescente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Relacion_Parentesco` FOREIGN KEY (`fkParentesco`) REFERENCES `parentesco` (`idParentesco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Relacion_Visitante` FOREIGN KEY (`fkVisitante`) REFERENCES `visitante` (`idVisitante`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `visita`
--
ALTER TABLE `visita`
  ADD CONSTRAINT `fk_Visita_Relacion` FOREIGN KEY (`fkRelVte`,`fkRelAdol`) REFERENCES `relacion` (`fkVisitante`, `fkAdolescente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Visita_Usuario` FOREIGN KEY (`fkUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `visitante`
--
ALTER TABLE `visitante`
  ADD CONSTRAINT `fk_Visitante_Municipio` FOREIGN KEY (`fkMunicipio`) REFERENCES `municipio` (`idMunicipio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Visitante_Nacionalidad` FOREIGN KEY (`fkNac`) REFERENCES `nacionalidad` (`idNacionalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
