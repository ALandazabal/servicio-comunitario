<?<?php 
	require_once("connection.php");
 ?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Highcharts Example</title>

		<style type="text/css">

		</style>
	</head>
	<body>
<script src="../../code/highcharts.js"></script>
<script src="../../code/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>



		<script type="text/javascript">

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Visitas Mensuales'
    },
    subtitle: {
        text: 'Separado por  Adolescente'
    },
    xAxis: {
        categories: [
            /*<?php 
            	$sql #= "SELECT * FROM visitante";
            	$result# = mysqli_query($connection,$sql);
            	#while($registros = mysqli_fetch_array($result)){
            	?>
            		'<?php# echo $registros["idVisitante"]; ?>',
            	<?php	
            	}
            ?>*/
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (mm)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Tokyo',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        /*data: [<?php 
            	$sql# = "SELECT * FROM visitante";
            	$result# = mysqli_query($connection,$sql);
            	#while($registros = mysqli_fetch_array($result)){
            	?>
            		<?php# echo $registros["fkNac"] ?>,
            	<?php	
            	}
             ?>]*/

    }]
});
		</script>
	</body>
</html>
