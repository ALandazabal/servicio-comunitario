<?php
/* @var $this AdolescenteController */
/* @var $model Adolescente */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'adolescente-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="pull-left span6">
		<div class="span8">
			<?php echo $form->labelEx($model,'fecha_ingreso'); ?>
			<?php 	date_default_timezone_set('America/Caracas');
 					$fecha = date("d-m-Y");
 					echo $form->textField($model,'fecha_ingreso',array('value'=>$fecha,'readonly'=>'readonly', 'class'=>'span12')); ?>
			<?php echo $form->error($model,'fecha_ingreso'); ?>
		</div>
		
		<div class="span8" style="display: none;">
			<?php echo $form->labelEx($model,'condicion'); ?>
			<?php echo $form->textField($model,'condicion',array('value'=>"presente",'readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'condicion'); ?>
		</div>
		<div class="span8">
			<?php echo $form->labelEx($model,'idAdolescente'); ?>
			<?php echo $form->textField($model,'idAdolescente',array('placeholder'=>'Ej. 21123123', 'class'=>'span12')); ?>
			<?php echo $form->error($model,'idAdolescente'); ?>
		</div>
		<div class="span8">
			<?php echo $form->labelEx($model,'fkNac'); ?>
			<?php echo $form->dropDownList($model,'fkNac',$model->getMenuNac(),array("empty"=>"--", 'class'=>'span12')); ?>
			<?php echo $form->error($model,'fkNac'); ?>
		</div>	
	</div>


	<div class="pull-left span6">
		<div class="span8">
			<?php echo $form->labelEx($model,'nombreA'); ?>
			<?php echo $form->textField($model,'nombreA',array('size'=>45,'maxlength'=>45, 'placeholder'=>'Ej. María', 'class'=>'span12')); ?>
			<?php echo $form->error($model,'nombreA'); ?>
		</div>
	
		<div class="span8">
			<?php echo $form->labelEx($model,'apellidoA'); ?>
			<?php echo $form->textField($model,'apellidoA',array('size'=>45,'maxlength'=>45, 'placeholder'=>'Ej. Mora', 'class'=>'span12')); ?>
			<?php echo $form->error($model,'apellidoA'); ?>
		</div>

		<div class="span8">
			<?php echo $form->labelEx($model,'direccion'); ?>
			<?php echo $form->textField($model,'direccion',array('size'=>100,'maxlength'=>100, 'placeholder'=>'Ej. Pueblo Nuevo', 'class'=>'span12')); ?>
			<?php echo $form->error($model,'direccion'); ?>
		</div>
	
		<div class="span8">
			<?php echo $form->labelEx($model,'telefono'); ?>
			<?php echo $form->textField($model,'telefono',array('placeholder'=>'04xxxxxxxxx', 'class'=>'span12')); ?>
			<?php echo $form->error($model,'telefono'); ?>
		</div>

	</div>

	

	<div class="buttons span12 center">
		<!-- <?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary btn-small span3 right')); ?> -->
		<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary btn-small span3 right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->