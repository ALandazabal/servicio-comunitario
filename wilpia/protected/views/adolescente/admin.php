<?php
/* @var $this AdolescenteController */
/* @var $model Adolescente */

$this->menu=array(
	array('label'=>'Reporte Excel', 'url'=>array('reporte'))
);

Yii::app()->clientScript->registerScript('search', "
$('.btn-info').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>

<h1>Adolescentes</h1>

<?php echo CHtml::link('Nuevo ingreso','#',array('class'=>'btn-info btn-small')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('create',array(
	'model'=>$model,
)); ?>

</div><!-- search-form -->


<?php $this->widget('zii.widgets.grid.CGridView', array(
	/*'id'=>'adolescente-grid',*/
	'dataProvider'=>$model->search(),
	/*'filter'=>$model,*/
	'columns'=>array(
		'idAdolescente',
		array(
			'name'=>'Nationality',//Nombre de la variable creada
			'value'=>'$data->fkNac0->descripcionN',//Nombres de la db
		),
		'nombreA',
		'apellidoA',
		'direccion',
		'telefono',
		'fecha_ingreso',
		'condicion',
		array(
			'header'=>'Liberar', 
			'class' => 'zii.widgets.grid.CButtonColumn',
			/*'htmlOptions' => array('style' => 'white-space: nowrap'),*/
			'template'=>'{update}',
			'updateButtonUrl' => 'Yii::app()->controller->createUrl("update",array("idAdolescente"=>$data["idAdolescente"]))',/**/
			'buttons' => array(
	    		'update' => array(
	        		'label' => 'Liberar',
	    			'visible' => '($data->condicion == "ausente" ) ? false :true',  //<< el condicional/* */
	    		),
    		),
		),		
	),
)); ?>
