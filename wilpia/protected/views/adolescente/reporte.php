<!--<picture>
	<source srcset="encabezado-reporte.png" media="(min-width:600px)">
</picture>
<img src="../encabezado.jpg" style="min-width:600px"/>-->
<h1>Reporte de Adolescentes</h1>
<p><?php date_default_timezone_set('America/Caracas');
 		$fecha = date("d-m-Y");
 		echo "Fecha: ".$fecha; ?>
 </p>
 <p><?php date_default_timezone_set('America/Caracas');
		$hora = date("g:i:s"); 
		echo "Hora: ".$hora; ?>
</p>
<table>
	<tr>
		<th style="background-color: #555; color: white">Cedula</th>
		<th style="background-color: #555; color: white">Nacionalidad</th>
		<th style="background-color: #555; color: white">Nombre</th>
		<th style="background-color: #555; color: white">Apellido</th>
		<th style="background-color: #555; color: white">Direccion</th>
		<th style="background-color: #555; color: white">Telefono</th>
		<th style="background-color: #555; color: white">Fecha ingreso</th>
		<th style="background-color: #555; color: white">Fecha salida</th>
	</tr>
	<?php foreach($model as $data): ?>
		<tr>
			<td><?php echo $data->idAdolescente ?></td>
			<td><?php 
				$var=Nacionalidad::model()->findAll("idNacionalidad=?", array($data->fkNac));
				echo $var[0]->descripcionN?>
					
			</td>
			<td><?php echo $data->nombreA ?></td>
			<td><?php echo $data->apellidoA ?></td>
			<td><?php echo $data->direccion ?></td>
			<td><?php echo $data->telefono ?></td>
			<td><?php echo $data->fecha_ingreso ?></td>
			<td><?php echo $data->fecha_salida ?></td>
		</tr>
	<?php endforeach; ?>
</table>
<p>Ministerio del Poder Popular para el Servicio Penitenciario</p>
<p>San Cristobal - Edo. Tachira - 2018</p>