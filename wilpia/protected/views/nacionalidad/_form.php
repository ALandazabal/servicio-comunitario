<?php
/* @var $this NacionalidadController */
/* @var $model Nacionalidad */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'nacionalidad-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">El campo con <span class="required">*</span> es requerido.</p>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'descripcionN'); ?>
		<?php echo $form->textField($model,'descripcionN',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'descripcionN'); ?>
	</div>

	<div class="buttons">
		<!-- <?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary btn-small span3 right')); ?> -->
		<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary btn-small span3 right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->