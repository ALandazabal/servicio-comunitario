<?php
/* @var $this NacionalidadController */
/* @var $model Nacionalidad */

/*$this->breadcrumbs=array(
	'Nacionalidads'=>array('admin'),
);

$this->menu=array(
	array('label'=>'Create Nacionalidad', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.btn-info').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?>

<h1>Nacionalidades</h1>

<?php echo CHtml::link('Nuevo ingreso','#',array('class'=>'btn-info btn-small')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('create',array(
	'model'=>$model,
)); ?>

</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'nacionalidad-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		/*'idNacionalidad',*/
		'descripcionN',
		array(
			'header'=>'Eliminar', 
			'class'=>'CButtonColumn',
    		'template'=>'{delete}',
		),
	),
)); ?>
