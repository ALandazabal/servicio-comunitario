<?php
/* @var $this RolController */
/* @var $model Rol */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'rol-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'descripcionR'); ?>
		<?php echo $form->textField($model,'descripcionR',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'descripcionR'); ?>
	</div>

	<div class="buttons">
		<!-- <?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary btn-small span3 right')); ?> -->
		<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary btn-small span3 right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->