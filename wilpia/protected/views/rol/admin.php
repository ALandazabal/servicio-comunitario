<?php
/* @var $this RolController */
/* @var $model Rol */

/*$this->breadcrumbs=array(
	'Rols'=>array('admin'),
);

$this->menu=array(
	array('label'=>'Create Rol', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.btn-info').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?>

<h1>Parentesco</h1>

<?php echo CHtml::link('Nuevo ingreso','#',array('class'=>'btn-info btn-small')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('create',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'rol-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		/*'idRol',*/
		'descripcionR',
		array(
			'header'=>'Eliminar', 
			'class'=>'CButtonColumn',
    		'template'=>'{delete}',
		),
	),
)); ?>
