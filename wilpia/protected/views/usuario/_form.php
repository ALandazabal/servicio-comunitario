<?php
/* @var $this UsuarioController */
/* @var $model Usuario */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuario-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'idUsuario'); ?>
		<?php echo $form->textField($model,'idUsuario',array('placeholder'=>'Inserte el número de cédula')); ?>
		<?php echo $form->error($model,'idUsuario'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'clave'); ?>
		<?php echo $form->passwordField($model,'clave',array('size'=>45,'maxlength'=>45, 'placeholder'=>'Inserte la contraseña')); ?>
		<?php echo $form->error($model,'clave'); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary btn-small span3 right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->