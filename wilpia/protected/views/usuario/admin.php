<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

/*$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
);*/

/*$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.btn-info').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?>

<h1>Orientadoras</h1>

<?php echo CHtml::link('Nuevo ingreso','#',array('class'=>'btn-info btn-small')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('create',array(
	'model'=>$model,
)); ?>

</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'usuario-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'idUsuario',
		array(
			'header'=>'Eliminar', 
			'class'=>'CButtonColumn',
    		'template'=>'{delete}',
		),
	),
)); ?>
