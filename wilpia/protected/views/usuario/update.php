<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

/*$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	$model->idUsuario=>array('view','id'=>$model->idUsuario),
	'Update',
);*/

$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Atras', 'url'=>array('admin')),
);
?>

<h1>Actualizar Usuario <?php echo $model->idUsuario; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>