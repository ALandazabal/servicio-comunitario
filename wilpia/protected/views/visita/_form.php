<?php
/* @var $this VisitanteController */
/* @var $model Visitante */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'visita-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="span5">
		<div class="span11">
			<?php echo $form->labelEx($model,'fecha'); ?>

			<?php date_default_timezone_set('America/Caracas');
 				$fecha = date("d-m-Y");
				echo $form->textField($model,'fecha',array('value'=>$fecha,'readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'fecha'); ?>
		</div>	

		<div class="span11">
			<?php echo $form->labelEx($model,'h_entrada'); ?>
			<?php date_default_timezone_set('America/Caracas');
					 $h_entrada = date("g:i:s");
				echo $form->textField($model,'h_entrada',array('value'=>$h_entrada,'readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'h_entrada'); ?>
		</div>	

		<div class="span11">
			<?php echo $form->labelEx($model,'fkUsuario'); ?>
			<!--<?php #echo $form->dropDownList($model,'fkUsuario',$model->getMenuUsuario(),array("empty"=>"--")); ?>-->
			<?php echo $form->textField($model,'fkUsuario',array('value'=>Yii::app()->user->name,'readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'fkUsuario'); ?>
		</div>
	</div>

	<div class="span6">
		<div class="span11">
			<!-- <?php echo $form->labelEx(Visitante::model(),'idVisitante'); ?> -->
			<?php echo $form->labelEx($model, 'cdVisitante'); ?>
			<!--<?php #echo $form->dropDownList($model,'fkRelVte',$model->getMenuVisitante(),array("empty"=>"--")); ?>-->
			<?php echo $form->textField(Visitante::model(),'idVisitante'); ?>
			<?php echo $form->error(Visitante::model(),'fkVisitante'); ?>
		</div>

		<div class="span11">
			<?php echo $form->labelEx($model, 'cdAdolescente'); ?>
			<!--<?php #echo $form->dropDownList($model,'fkRelAdol',$model->getMenuAdolescente(),array("empty"=>"--")); ?>-->
			<?php echo $form->textField(Adolescente::model(),'idAdolescente'); ?>
			<?php echo $form->error(Adolescente::model(),'fkAdolescente'); ?>
		</div>
		<div class="span11">
			<?php echo $form->labelEx($model, 'parentesco'); ?>
			<?php echo $form->dropDownList(Rol::model(),'idRol',$model->getMenuRol()); ?>
			<?php echo $form->error(Rol::model(),'idRol'); ?>
		</div>
	</div>

	<div class="buttons span12 center">
		<!-- <?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-primary btn-small span3 center')); ?>
			 -->	
		<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-primary btn-small span3 center')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->