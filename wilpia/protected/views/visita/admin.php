<?php
/* @var $this VisitaController */


$this->menu=array(
	array('label'=>'Reporte Excel', 'url'=>array('reporte')),
);

Yii::app()->clientScript->registerScript('search', "
$('.btn-info').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<h1>Visita</h1>

<?php echo CHtml::link('Nuevo ingreso','#',array('class'=>'btn-info btn-small')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('create',array(
	'model'=>$model,
)); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	/*'id'=>'visita-grid',*/
	'dataProvider'=>$model->search(),
	/*'summaryText'=>'',
	'filter'=>$model,*/
	'columns'=>array(
		'fecha',
		'h_entrada',
		'h_salida',	
		'fkUsuario',
		array(
			'name'=>'Visitante', 
			'type'=>'raw', 
			'value'=>function($model){
				$var = $model->getMenuVisitanteR($model->fkRelVte);
				//$var2=Adolescente::model()->findAll("idAdolescente=?", $model->fkRelAdol);
				//echo 
				return $var;//$var;
			},
		),
		array(
			'name'=>'Adolescente', 
			'type'=>'raw', 
			'value'=>function($model){
				$var = $model->getMenuAdolescenteR($model->fkRelAdol);
				//$var2=Adolescente::model()->findAll("idAdolescente=?", $model->fkRelAdol);
				//echo 
				return $var;//$var;
			},
		),
		array(
			'name'=>'Parentesco', 
			'type'=>'raw', 
			'value'=>function($model){
				$var = $model->getMenuRelacion($model->fkRelVte,$model->fkRelAdol);
				$var2=Rol::model()->findAll("idRol=?", array($var));
				//echo 
				return $var2[0]->descripcionR;//$var;
			},
			/*'$data->fkRol0->fkRol',CHtml::encode(function($data,$row){
				$criteria = new CDbCriteria;
				$criteria->condition='fkVisitante=:fkVte AND fkAdolescente=:fkAdol';
				$criteria->params=array(':fkVte'=>$data->fkRelVte, ':fkAdol'=>$data->fkRelAdol);
				echo "<script> alert('".$data['fkRelVte']."')</script>";
				return Relacion::model()->find($criteria);
			}),*/
		),
			/*'Relacion::model()->find('fkVisitante=:fkVte AND fkAdolescente=:fkAdol', array(':fkVte'=>$data['fkRelVte'], ':fkAdol'=>$data['fkRelAdol']))',
		),	"Check",$data->stopPublish,array("id"=>"chkPublish_".$data->id)*/
		/*array(
			'name'=>'Rolv',
			'value'=>'$data->fkRol0->idRol',
		),*/
		array(
			'header'=>'Salida', 
			'class' => 'zii.widgets.grid.CButtonColumn',
			/*'htmlOptions' => array('style' => 'white-space: nowrap'),*/
			'template'=>'{update}',
			'updateButtonUrl' => 'Yii::app()->controller->createUrl("update",array("fecha"=>$data["fecha"],"fkRelVte"=>$data["fkRelVte"],"fkRelAdol"=>$data["fkRelAdol"]))',
			'buttons' => array(
	    		'update' => array(
	        		'label' => 'Salir',
	    			'visible' => '($data->h_salida != null ) ? false : true',  //<< el condicional
	    		),
    		),
		),/**/
	),
));
	
	/*$arra = array();
	$var2=Adolescente::model()->findAll();
	foreach($var2 as $reg){
		array_push($arra, $reg->idAdolescente);
	}

	$xAxis = array(1,2,3);
    $yAxis = array(4,5,6);

$this->Widget('ext.yii-highcharts.highcharts.HighchartsWidget', array(
'options' => array(
'title' => array('text' => 'Número de Visitas al mes'),
'xAxis' => array(
'categories' => $arra,
),
'yAxis' => array(
'title' => array('text' => 'Cantidad')
),
'series' => array(
array('name' => 'Jane', 'data' => $arra),
array('name' => 'John', 'data' => array(5, 7, 3))
)
)
));*/
  ?>

  
<!-- <canvas id="popChart" width="600" height="400"></canvas> -->