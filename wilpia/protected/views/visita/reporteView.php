<!-- <img src="../encabezado.jpg" style="min-width:600px"/> -->
<h1>Reporte de Visitas</h1>
<p><?php date_default_timezone_set('America/Caracas');
 		$fecha = date("d-m-Y");
 		echo "Fecha: ".$fecha; ?>
 </p>
 <p><?php date_default_timezone_set('America/Caracas');
		$hora = date("g:i:s"); 
		echo "Hora: ".$hora; ?>
</p>

<table>
	<tr>
		<th style="background-color: #555; color: white">Fecha</th>
		<th style="background-color: #555; color: white">Hora Entrada</th>
		<th style="background-color: #555; color: white">Hora Salida</th>
		<th style="background-color: #555; color: white">Usuario</th>
		<th style="background-color: #555; color: white">Visitante</th>
		<th style="background-color: #555; color: white">Adolescente</th>
		<th style="background-color: #555; color: white">Parentesco</th>
	</tr>
	<?php foreach($model as $data): ?>
	<tr>
		<td><?php echo $data->fecha?></td>
		<td><?php echo $data->h_entrada?></td>
		<td><?php echo $data->h_salida?></td>
		<td><?php echo $data->fkUsuario?></td>
		<td><?php echo $data->fkRelVte?></td>
		<td><?php echo $data->fkRelAdol?></td>
		<td><?php 
			$var = $data->getMenuRelacion($data->fkRelVte,$data->fkRelAdol);
			$var2=Rol::model()->findAll("idRol=?", array($var));
			echo $var2[0]->descripcionR?></td>
	</tr>
	<?php endforeach; ?>
</table>
<p>Ministerio del Poder Popular para el Servicio Penitenciario</p>
<p>San Cristobal - Edo. Tachira - 2018</p>