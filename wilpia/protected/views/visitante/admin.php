<?php
/* @var $this VisitanteController */
/* @var $model Visitante */

$this->menu=array(
	array('label'=>'Reporte Excel', 'url'=>array('reporte')),
);

Yii::app()->clientScript->registerScript('search', "
$('.btn-info').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>

<h1>Visitantes</h1>

<?php echo CHtml::link('Nuevo ingreso','#',array('class'=>'btn-info btn-small')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('create',array(
	'model'=>$model,
)); ?>
<!--<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>search-form -->
</div> 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'visitante-grid',
	'dataProvider'=>$model->search(),
	'summaryText'=>'',
	'filter'=>$model,
	'columns'=>array(
		'idVisitante',
		'nombreV',
		'apellidoV',
		'direccion',
		'telefono',
		//'fkMunicipio',
		//'fkNac',
		array(
			'name'=>'Nationality',
			'value'=>'$data->fkNac0->descripcionN',
		),
		array(
			'name'=>'Municip',
			'value'=>'$data->fkMunicipio0->descripcionM',
		),
		array(
			'header'=>'Estado', 
			'type'=>'raw',
			'value'=>function($model){
				$var=Municipio::model()->findAll("idMunicipio=?", array($model->fkMunicipio));
				$var2=Estado::model()->findAll("idEstado=?",array($var[0]->fkEstado));
				return $var2[0]->descripcionE;//$var;
			},
		),
		/*array(
			'class' => 'CButtonColumn',
			'htmlOptions' => array('style' => 'white-space: nowrap'),
			'template'=>'{delete}{update}',
		),*/
	),
)); ?>
