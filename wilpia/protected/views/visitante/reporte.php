<!-- <img src="../encabezado.jpg" style="min-width:600px"/> -->
<h1>Reporte de Visitantes</h1>
<p><?php date_default_timezone_set('America/Caracas');
 		$fecha = date("d-m-Y");
 		echo "Fecha: ".$fecha; ?>
 </p>
 <p><?php date_default_timezone_set('America/Caracas');
		$hora = date("g:i:s"); 
		echo "Hora: ".$hora; ?>
</p>
<table>
	<tr>
		<th style="background-color: #555; color: white">Cedula</th>
		<th style="background-color: #555; color: white">Nombre</th>
		<th style="background-color: #555; color: white">Apellido</th>
		<th style="background-color: #555; color: white">Direccion</th>
		<th style="background-color: #555; color: white">Telefono</th>
		<th style="background-color: #555; color: white">Estado</th>
		<th style="background-color: #555; color: white">Municipio</th>
		<th style="background-color: #555; color: white">Nacionalidad</th>
	</tr>
	<?php foreach($model as $data): ?>
		<tr>
			<td><?php echo $data->idVisitante ?></td>
			<td><?php echo $data->nombreV ?></td>
			<td><?php echo $data->apellidoV ?></td>
			<td><?php echo $data->direccion ?></td>
			<td><?php echo $data->telefono ?></td>
			<!--<td><?php #echo $data->telefono ?></td>-->
			<td><?php 
				$var = Municipio::model()->findByPk($data->fkMunicipio);
				$var2 = Estado::model()->findAll("idEstado=?",array($var->fkEstado));
				echo $var2[0]->descripcionE ?></td>
			<td><?php
				$var = Municipio::model()->findByPk($data->fkMunicipio);
				echo $var->descripcionM ?></td>
			<td><?php
				$var = Nacionalidad::model()->findByPk($data->fkNac);
				echo $var->descripcionN ?></td>
		</tr>
	<?php endforeach; ?>
</table>
<p>Ministerio del Poder Popular para el Servicio Penitenciario</p>
<p>San Cristobal - Edo. Tachira - 2018</p>